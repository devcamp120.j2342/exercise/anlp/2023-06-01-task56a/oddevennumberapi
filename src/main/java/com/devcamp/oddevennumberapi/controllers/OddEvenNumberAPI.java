package com.devcamp.oddevennumberapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class OddEvenNumberAPI {
    @GetMapping("/checknumber")
    public String checkOddEvenNumber(@RequestParam(required =true, name = "number") int requestNumber){
        if (requestNumber %2 == 0){
            return "Số " + requestNumber + " là số chẵn";
        } else {
            return "Số " + requestNumber + " là số lẻ";
        }
    }
}
